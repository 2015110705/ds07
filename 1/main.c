//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define MAX_STACK_SIZE 100
#define TRUE 1
#define FALSE 0

typedef struct {
	short int vert;
	short int horiz;
} offsets;

typedef struct {
	short int row;
	short int col;
	short int dir;
} element;

int maze[12][12];						//미로 배열
int mark[12][12];						//방문했는지 체크하는 배열
const offsets move[8] =					//8방향 이동크기 배열(상수로 지정)
{
	{-1, 0}, {-1, 1}, {0, 1}, {1, 1},
	{1, 0}, {1, -1}, {0, -1}, {-1, -1}
};						
element stack[MAX_STACK_SIZE];			//스택
int top = -1;							//스택 - 탑
int EXIT_ROW, EXIT_COL;					//탈출 지점 좌표

void path();
element pop();
void push(element);

int main()
{
	FILE* pInputFile = fopen("input.txt", "r");			//파일 포인터
	int i, j;

	fscanf(pInputFile, "%d %d", &EXIT_ROW, &EXIT_COL);	//출구 데이터 입력
	
	//미로 데이터 입력
	for (i = 1; i <= EXIT_ROW; i++) {
		for (j = 1; j <= EXIT_COL; j++) {
			fscanf(pInputFile, "%d", &maze[i][j]);
		}
	}
	
	//파일 닫기
	fclose(pInputFile);
	
	//미로 배열 테두리 1로 지정
	for (i = 0; i <= EXIT_COL + 1; i++) {
		maze[0][i] = 1;
		maze[EXIT_ROW + 1][i] = 1;
	}
	for (i = 1; i <= EXIT_ROW; i++) {
		maze[i][0] = 1;
		maze[i][EXIT_COL + 1] = 1;
	}

	//마크 배열 초기화
	for (i = 0; i <= EXIT_ROW; i++) {
		for (j = 0; j <= EXIT_COL; j++) {
			mark[i][j] = 0;
		}
	}

	//길찾기
	path();

	return 0;
}

//길찾기 함수
void path()
{
	/* output a path through the maze if such a path exists */
	int i, row, col, nextRow, nextCol, dir, found = FALSE;
	element position;
	mark[1][1] = 1; top = 0;
	stack[0].row = 1; stack[0].col = 1; stack[0].dir = 1;
	
	//탐색 완료되기 전까지 탐색
	while (top > -1 && !found) {
		position = pop();
		row = position.row; col = position.col;
		dir = position.dir;

		while (dir < 8 && !found) {
			/* move in direction dir */
			nextRow = row + move[dir].vert;
			nextCol = col + move[dir].horiz;

			//출구에 다다랐을 때 found = true로 설정
			if (nextRow == EXIT_ROW && nextCol == EXIT_COL) {
				found = TRUE;
			}
			else if ( ! maze[nextRow][nextCol] && 
				! mark[nextRow][nextCol]) {
				mark[nextRow][nextCol] = 1;
				position.row = row; position.col = col;
				position.dir = ++dir;
				push(position);
				row = nextRow; col = nextCol; dir = 0;
			}
			else ++dir;
		}
	}

	//결과 출력
	if (found) {
		printf("The path is : \n");
		printf("row	 col \n");
		for (i = 0; i <= top; i++) {
			printf("%2d%5d\n", stack[i].row, stack[i].col);
		}
		printf("%2d%5d\n", row, col);
		printf("%2d%5d\n", EXIT_ROW, EXIT_COL);
	}
	else printf("The maze does not have a path \n");
}

//스택 - 팝 함수
element pop()
{
	element _pop = stack[top];
	top--;

	//탑을 1 감소후 그 값을 리턴
	return _pop;
}

//스택 - 푸시 함수
void push(element _element)
{
	//탑을 1증가후 푸시
	top++;
	stack[top] = _element;
}