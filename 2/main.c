//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#define MAX_STACK_SIZE 100
#define MAX_EXPR_SIZE 100

//enum - 연산자 우선순위
typedef enum {lparen, rparen, plus, minus, times, divide, 
	mod, eos, operand} precedence;		

int top = -1;
int stack[MAX_STACK_SIZE];
char expr[MAX_EXPR_SIZE];

int eval();
precedence getToken(char *symbol, int *n);
void push(int _value);
int pop();

int main()
{
	FILE* pInputFile = fopen("input.txt", "r");

	//postfix expression 입력(expr)
	fscanf(pInputFile, "%s", expr);

	//파일 닫기
	fclose(pInputFile);

	//입력 값 출력
	printf("post fix expression : ");
	printf("%s \n", expr);

	//계산 후 출력
	printf("The ealuation value : %d \n", eval());

	return 0;
}

int eval()
{
	/* evaluate a postfix expression, expr, maintained as a 
	global variable, '\0' is the the end of the expression.
	The stack and top of the stack are global variables.
	getToken is used to return the token type and 
	the character symbol. Operands are assumed to be single character digits */

	precedence token;
	char symbol;
	int op1, op2;
	int n = 0;
	top = -1;
	token = getToken(&symbol, &n);
	
	//토큰이 end of statement가 아닌지 판별
	while (token != eos) {
		if (token == operand) {
			push(symbol - '0');				//stack insert(한자리숫자 alpha -> integer)
		}
		else {
			/* pop two operands, perform operation, and
			push result to the stack*/
			op2 = pop();
			op1 = pop();
			switch (token) {
			case plus : push(op1 + op2);
				break;
			case minus : push(op1 - op2);
				break;
			case times : push(op1 * op2);
				break;
			case divide : push(op1/op2);
				break;
			case mod : push(op1 % op2);
				break;
			}
		}
		//토큰 받아오기
		token = getToken(&symbol, &n);
	}

	return pop();		//return result;
}

//토큰의 우선순위 가져오기
precedence getToken(char *symbol, int *n)
{
	/* get the next token, symbol is the character
	representation, which is returned, the token is
	represented by its enumerated value, 
	which is returned in the function name*/
	*symbol = expr[(*n)++];
	switch (*symbol) {
	case '(' : return lparen;
	case ')' : return rparen;
	case '+' : return plus;
	case '-' : return minus;
	case '/' : return divide;
	case '*' : return times;
	case '%' : return mod;
	case '\0' :return eos;
	default : return operand;
	}
}

void push(int _value)
{
	//탑을 1 증가한 후에 값을 대입
	top++;
	stack[top] = _value;
}

int pop()
{
	//탑을 1감소후 팝한 값을 리턴
	int _pop = stack[top];
	top--;

	return _pop;
}